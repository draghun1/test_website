---
title: Deepthi Raghunandan
subtitle: Computer Scientist
---

### Education: M.S. Computer Science, University of Maryland

{{< figure src="/img/earth_overview.jpeg" title="deepthi" width=250px height=250px >}}

Email: deepthi.raghunandan@nasa.gov