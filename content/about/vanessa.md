---
title: Vanessa Valenti 
subtitle: Computer Scientist 
---

### Education: BSc Geography/Environmental Studies (University of California, Los Angeles)

{{< figure src="/img/earth_overview.jpeg" title="foo" width=250px height=250px >}}

Email: vanessa.valenti@nasa.gov
