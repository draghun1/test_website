---
title: Jules Kouatchou
subtitle: Computational Scientist
---

### Education: Ph.D Scientific Computing (George Washington University)

{{< figure src="/img/jules_pic.png" title="foo" width=350 height=350 >}}

Dr. Jules Kouatchou has more than 18 years’ experience implementing atmospheric models on high performance computers. He has developed Python tools for running independent tasks on distributed systems, for pre- & post-processing large volume of data, for visualizing data and for carrying out regression tests. In recent years, Dr. Kouatchou has initiated an informal project which goal is to compare the performance of languages such as Python, Julia, Matlab, IDL, R, Java, Scala, C & Fortran. He has been teaching (to NASA employees) Python related courses over the past 10 years.