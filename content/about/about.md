---
title: About Us
---

We are a team of highly skilled individuals capable of developing and applying high-performance computing solutions, integrating and deploying advanced software techniques to improve NASA’s Earth Science models, providing advanced software support and training, and conducting research in emerging computing technologies and data analytics.​

# Members

## [Bruce van Aartsen]({{< ref "/about/bruce" >}})

*Bruce van Aartsen* has over 25 years of experience in weather applications development, including model visualization for the Air Force Meteorological Models office. At ASTG, he co-authored software coding standards, developed and maintained Java/HTML code for NASA web-apps, and devised efficient workflow scripts for the DDSCAT radiation model. Recent development includes new Ent land-surface drivers for GISS ModelE, and enhancements to GISS’ Regression Testing System.

---
## [Maharaj Bhat]({{< ref "/about/bhat" >}})

---
## [Purnendu Chakraborty]({{< ref "/about/purnendu" >}})

*Dr. Purnendu Chakraborty* is an HPC applications developer with a particular interest in new technologies. He is currently working on enabling GEOS simulations on exascale architectures.

---
## [Carlos Cruz]({{< ref "/about/carlos" >}})

*Dr. Carlos Cruz*, is a senior computational scientist in the *ASTG*. with over 20 years of experience developing and providing *HPC* software solutions for NASA's Earth system models including *GEOS*, *LIS*, *NU-WRF*, and *GISS* Model E. He is currently the lead software developer for *NU-WRF* and also supports visualizations efforts for the Chemistry Climate Modeling (*CCM*) group. 


---
## [Megan Damon]({{< ref "/about/megan" >}})

*Megan Damon* is a computer scientist and has been developing scientific codes and workflows at GSFC, NASA since 2005. She has achieved a master’s degree beyond and specializes in high performance computing, operational development, and large-scale visualization tactics.

---
## [Jules Kouatchou]({{< ref "/about/jules" >}})

*Dr. Jules Kouatchou* has been working on implementing atmospheric models on high performance computers. He is involved in maintaining the GEOS CTM model and improving the computing capabilities (adding ESMF regridding and Parallel IO) of the LIS model. He has developed Python tools for running independent talks on distributed systems, for pre-processing and post-processing large datasets, for visualizing data, and carrying out regression tests. In recent years, Dr. Kouatchou has initiated an informal project which goal is to compare the performance of languages (Python, Julia, Matlab, R, IDL, Java, Scala, C, Fortran). He has been organizing Python training courses for NASA employees and for NASA GSFC summer interns.

---
## [Christopher Kung]({{< ref "/about/chris" >}})

*Dr. Christopher Kung* is a Software System's Engineer that has over 15 years of experience in the realm of high performance computing at the DoD and NASA.  At the DoD, he worked with researchers and scientists to port and optimize computational electromagnetics and acoustics codes for execution on the DoD high performance computing resources.  For NASA, he is currently investigating domain specific languages as a potential path to incorporate accelerator-based computing into NASA's GEOS codebase.

---
## [Craig Pelissier]({{< ref "/about/craig" >}})

*Dr. Craig. Pelissier*, has several years of experience managing software projects that support NASA *GSFC* Earth system models such as *GEOS*,  *LIS*, *NU-WRF*, and the *GISS* Model E. He is currently the lead of the *ASTG*, a group of ten computer scientists, that provide software solutions several Earth system models. He also actively conducts research in *ML* land surface modeling [references] and data science, has extensive experience developing *HPC* enabled scientific software, and a strong technical background with a doctoral thesis on large simulations of Quantum field theories. 

---
## [Deepthi Raghunandan]({{< ref "/about/deepthi" >}})

*Deepthi Raghunandan* is a member of *ASTG* working towards a Ph.D in Computer Science. Her thesis pertains to improving data science tools geared towards teaching. She currently holds a Master's degree in Computer Science from the University of Maryland. When she's not working on her Ph.D dissertation, she's thinking about working on her dissertation. 

---
## [Vanessa Valenti]({{< ref "/about/vanessa" >}})

*Vanessa Valenti* is a recent member of the *ASTG* with a B.S. in Environmental Studies. She is currently developing web-based *UI* that leverage the Python ecosystem (e.g., Holoviews, Bokeh, and Panels) that provide fast, easy, powerful tools for visualization and data analysis of model products and also supports computational efforts for the Chemistry Climate Modeling (*CCM*) group. 

