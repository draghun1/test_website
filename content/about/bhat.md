---
title: Maharaj Bhat
subtitle: Computational Scientist
---

### Education: Ph.D Computational Fluid Dynamics (University of Tennessee)

{{< figure src="/img/earth_overview.jpeg" title="Bhat" width=350px height=350px >}}

Email: maharaj.k.bhat@nasa.gov
