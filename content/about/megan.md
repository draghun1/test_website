---
title: Megan Damon
subtitle: Computer Scientist 
---

### Education: MSc Computer Science

{{< figure src="/img/earth_overview.jpeg" title="foo" width=250px height=250px >}}

Email: megan.r.damon@nasa.gov
