---
title: Carlos A. Cruz
subtitle: Computational Scientist
---

### Education: Ph.D Climate Dynamics (George Mason University)

{{< figure src="/img/earth_overview.jpeg" title="foo" width=250px height=250px >}}


Email: carlos.a.cruz@nasa.gov
Projects (Role): 
	Lead NUWRF Support
	Lead eViz/iViz
	Software Training
Expertise:  High Performance Computing, Software Engineering
            Languages: Fortran, Python
Selected publications/presentations: Highlight the papers which highlight your background. (+NTR)
Bio: Dr. Carlos Cruz is a computational scientist at SSAI and NASA GSFC. For over 20 years he has been involved in all aspects of software development while supporting several earth system models including GEOS, GISS modelE and NU-WRF. He is currently the lead software developer for \cgls{NU-WRF} and manages the support of several MAP proposals that utilize the NU-WRF framework. He is developing, jointly with V. Valenti, the earth system modeling visualization tool, eViz, that will be used to support this project. Dr. Cruz is also an adjunct professor of Computational and Data Sciences at George Mason University where he teaches Modeling and Simulation since 2018.
