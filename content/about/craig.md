---
title: Craig S. Pelissier
subtitle: Physicist
---

### Education: Ph.D Physics (George Washington University)

{{< figure src="/img/earth_overview.jpeg" title="foo" width=250px height=250px >}}

Email: craig.s.pelissier@nasa.gov

*Projects* (Role): 

    - Lead ASTG
    - Research & Development  
        - Earth Systems Modelling (ML/AI Land Surface Modelling)
        - Precipitation Measurment Mission (Algorithm team)
        - Quantum Computing

---

*Expertise*: 
    Large Numeral Simulations, High Performance Computing, 
    Maching Learning, Electromagnetic Scattering   

---

*Languages*: C/C++, Python, Shell

---

*Selected publications/presentations*:

    Nearing, Grey S., Frederik Kratzert, Alden Keefe Sampson, *Craig S. Pelissier*, Daniel Klotz, Jonathan M. Frame, Cristina Prieto, and Hoshin V. Gupta. "What role does hydrological science play in the age of machine learning?." Water Resources Research 57, no. 3, (2021). 
    *C. Pelissier*, T. Ames and J. Le Moigne, "Quantum Assisted Image Registration," IGARSS 2020 - 2020 IEEE International Geoscience and Remote Sensing Symposium, 2020, pp. 3696-3699, (2020). 
    *C. Pelissier*, J. Frame and G. Nearing, "Combining Parametric Land Surface Models with Machine Learning," IGARSS 2020 - 2020 IEEE International Geoscience and Remote Sensing Symposium, 2020, pp. 3668-3671, (2020). 
    Mulligan, Vikram Khipple, Hans Melo, Haley Irene Merritt, Stewart Slocum, Brian D. Weitzner, Andrew M. Watkins, P. Douglas Renfrew, *Craig Pelissier*, Paramjit S. Arora, and Richard Bonneau. "Designing peptides on a quantum computer." bioRxiv: 752485, (2020). 
    *C. Pelissier*, Quantum Assisted Learning for Image Registration, American Geophysical Union, (2017). 
    *C. Pelissier*, Machine Learning in Complex Earth System Models, Super Computing, (2017) 

---

*Bio*: 

Craig Pelissier joined NASA in 2013 after completing a PhD in numerical simulations of Quantum Chromodynamics.  In 2018, he became the lead of the ASTG where he manages a diverse portfolio of software projects. He also conducts research in machine learning for land surface modeling, supports NASA's Precipitation Measurement Mission developing numerical simulations to melt frozen hydrometeors and compute their radiative properties to advance state-of-the-art models.  He is an avid badminton player, enjoys playing the piano, and spending time outdoors.