---
title: Purnendu Chakraborty
subtitle: Software Engineer
---

### Education: Ph.D Computer Science (University of Maryland)

{{< figure src="/img/earth_overview.jpeg" title="foo" width=250px height=250px >}}

Email: purnendu.chakraborty@nasa.gov