| . | . |
| - | - |
| ![NASA](http://www.nasa.gov/sites/all/themes/custom/nasatwo/images/nasa-logo.svg) | ![NCCS](https://www.nccs.nasa.gov/sites/default/files/NCCS_Logo_0.png) |

**Registration for this course is done through SATERN:**

- **SATERN Course ID**: GSFC-ITF 
- **SATERN Course Title**: INTRODUCTION TO FORTRAN 
- **Course Dates**: October 20 & 22 (10:00-15:30 US EST)
- [CLICK HERE to REGISTER](https://hcm03.ns2cloud.com/sf/learning?destUrl=https%3a%2f%2fnasa%2dhcm03%2ens2cloud%2ecom%2flearning%2fuser%2fdeeplink%5fredirect%2ejsp%3flinkId%3dREGISTRATION%26scheduleID%3d131224%26fromSF%3dY&company=NASAHCM03)

# Introduction to Fortran

Fortran is a computer programming language that is extensively used in numerical and scientific computing. It has a strong user base with scientific programmers, and is used to develop weather forecasting and financial trading models as well as engineering simulations. Fortran programs can be highly optimized to run on high performance computers, and in general the language is suited to producing code where performance is important.

Fortran is a compiled language, or more specifically it is compiled ahead-of-time. In other words, you must perform a special step called compilation that translates your written code before you are able to run it on a computer. This is where Fortran differs with interpreted languages such as Python and R which run through an interpreter that in turn executes the instructions directly, but at the cost of compute speed.

In this two-day course, we will introduce the main concepts of the Fortran programming language. We will learn how to write simple Fortran programs and will design and write a complex application. Throughout this hands-on class, we will solve several  exercises to reinforce what is presented.

### Target Audience
This course will be useful for scientists and engineers who want to start programming in Fortran or transition from Fortran 77 to Fortran 90.

### Prerequisites
Familiarity with computer programming, and if possible any compiled language. You will be asked to provide your gmail userid in order to be granted access to the NASA Center for Climate Simulation (NCCS) Science Data Managed Cloud Environment (SMCE).

### Expected Outcomes
After this workshop, participants will be able to:
- Understand 
    - Fortran data types
    - How to perform arithmetic operations
    - How to write conditional statements, loops
    - How to write subroutines, functions and modules
    - How to manipulate data array
    - The different components of a Fortran programs
- Write a Fortran application (including functions/subroutines) from scratch
- Read/Write files
- Compile and run the application.
- Understand how to write a Makefile file needed to compile a Fortran application.

### Jupyter Notebook

You might find it useful to learn little more about Jupyter notebook:
 [![Open In Colab](https://colab.research.google.com/assets/colab-badge.svg)](https://colab.research.google.com/github/astg606/py_materials/blob/master/jupyter_notebook/jupyter_notebook_introduction.ipynb)

## Teaching Tool and Platform

All the materials are written in Jupyter notebook files to facilitate accessibility, readability and maintainability. Whenever necessary, we may write and execute simple Fortran programs directly from Jupyter notebook cells. Most the times, we will use command line to write, compile and execute Fortran programs.

The teaching platform will be the NASA Center for Climate Simulation (NCCS) Science Data Managed Cloud Environment (SMCE). It is a cloud based system that is accessible through a browser.
To log in to SMCE, click on the link:

[https://astg.mysmce.com](https://astg.mysmce.com)

It is assumed that you have already provided your gmail userid.
You will use your gmail credential to automatically access the system.
It is recommended that you first use a browser to  access your gmail account and then click on the link above.

If you are not familiar yet with `vi`, we recommend that you learn the basic `vi` commands. 
You can start with the following tutorial: [Vi Text Editor!](https://ryanstutorials.net/linuxtutorial/vi.php).

#### Git

It might be useful to learn how to do version control with Git. Please check the following tutorials:

- Tutorial on Google Colab: [![Open In Colab](https://colab.research.google.com/assets/colab-badge.svg)](https://colab.research.google.com/github/astg606/py_materials/blob/master/git_tutorial/basic_git_tutorial.ipynb)
- [Software Carpentry](https://swcarpentry.github.io/git-novice/)
 
## Topics
The following topics will be covered:

#### Day 1: Wednesday, October 20: 10:00-15:30 US EST

- [Introduction](https://git.mysmce.com/astg/training/for_materials/-/blob/intro_2day/intro_notebooks/01_introduction_fortran.ipynb)
    - History of Fortran
    - Compilers
    - First Fortran Program
    - Fortran Program Structure
- [Variables and Data Types](https://git.mysmce.com/astg/training/for_materials/-/blob/intro_2day/intro_notebooks/02_data_types.ipynb)
    - Variable Naming Convention and Declaration
    - Integer Data Type
    - Real Data Type
    - Logical Data Type
    - Character Data Type
    - Complex Data Type
- [Conditionals and Loops](https://git.mysmce.com/astg/training/for_materials/-/blob/intro_2day/intro_notebooks/03_conditionals_and_loops.ipynb)
    - If-Else
    - Select Case
    - Do Loops
    - While Loops
    - Loops: Exit and Cycle
- [Array Concepts](https://git.mysmce.com/astg/training/for_materials/-/blob/intro_2day/intro_notebooks/04_array_concepts.ipynb)
    - Declaring and Initializing Arrays
    - Array Constructors
    - Array Slicing
    - Where Structure

#### Day 2: Friday, October 22: 10:00-15:30 US EST

- [Subroutines and Functions](https://git.mysmce.com/astg/training/for_materials/-/blob/intro_2day/intro_notebooks/05_functions_subroutines.ipynb)
    - User Defined Subroutines
    - Keyword Calls and Optional Arguments
    - User Defined Functions
    - Intrinsic Function
- [Modules and Interfaces](https://git.mysmce.com/astg/training/for_materials/-/blob/intro_2day/intro_notebooks/06_modules_interfaces.ipynb)
    - Module Procedures
    - Public and Private Accessibility
    - Explicit Interfaces
- [File IO](https://git.mysmce.com/astg/training/for_materials/-/blob/intro_2day/intro_notebooks/07_file_io.ipynb)
    - Internal File IO
    - Formatted IO
    - Non-Advancing IO
    - Namelist
- [Derived Types and Pointers](https://git.mysmce.com/astg/training/for_materials/-/blob/intro_2day/intro_notebooks/08_derived_types_and_pointers.ipynb)
    - Derived Types
         - Declaration
         - Defining values
         - Component Selection
         - Nesting Derived Types
         - Use in modules
    - Pointers
         - Tartget and Association
         - Array Pointers
         - Linked List

<!---
| Date | Time | Topic | Web Link |
| --- | --- | --- | --- |
| May 19, 2021 | 08:30-16:30 | **Introduction to Fortran** | [Introduction to Fortran](https://git.mysmce.com/astg/training/for_courses/-/tree/master/introduction) |
| May 20, 2021 | 09:00-16:00 | **Modern Fortran (Object-Oriented and Interoperability)** | [Modern Fortran (Object-Oriented and Interoperability)](https://git.mysmce.com/astg/training/for_courses/-/tree/master/modern) |
--->

## Obtaining the materials
The materials are in a Git repository. To obtain them from your local platform, issue the command:

    git clone -b intro_2day https://git.mysmce.com/astg/training/for_materials.git

You will have a directory named `for_materials`. If you want to obtain new updates, go to `for_materials` and type:

     git stash
     git pull

To obtain the materials that contain the solutions to exercises, type:

     git clone -b solutions https://git.mysmce.com/astg/training/for_materials.git

## Useful Resources

- [Introduction to Computer Programming Using Fortran 95](https://www.archer.ac.uk/training/course-material/2014/08/F95_CCFE/Fortran95CourseNotes.pdf)
- [Introduction to Programming using Fortran 95/2003/2008](http://www.egr.unlv.edu/~ed/fortranv3.pdf)
- [Introduction to Modern Fortran](https://www-uxsup.csx.cam.ac.uk/courses/moved.Fortran/)

<!---
## Evaluation Survey
At the end of the class, please fill out the [EVALUATION SURVEY](https://forms.gle/ENREZho3f5n9FEP39). Thank you in advance.
--->

## Instructors
This class was prepared and will be presented by:

- Carlos Cruz (Carlos.Cruz@nasa.gov)
- Jules Kouatchou (Jules.Kouatchou@nasa.gov)
