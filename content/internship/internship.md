2021:  

    Assessing the value of remote sensing data in deep learning hydrological models, Logan Qualls, Masters candidate, University of Alabama. 

    Eviz: Exploring Earth System Data Products and diagnosing Earth System Models Using a Visualization Environment, Deepthi Raghunandan, PhD candidate, University of Maryland. 

2021:  

    Jupyter Notebook Extensions, Deepthi Raghunandan, PhD candidate, University of Maryland. 

2019:  

    Data Assimilation into Machine Learning Dynamical Hydrology Models, Jonathan Frame, PhD candidate University of Alabama. 

    Quantum Annealing to Design Synthetic Proteins, Stewey Slocum, B.S. candidate, John Hopkins University. 

    Quantum Linear-System Solvers on the IBMQ Quantum Computer, Chris Culver, PhD Candidate, The George Washington University. 

2018:  

    An Assessment of the IBMQ Quantum Computer for Solving Linear Systems, Chris Culver, PhD Candidate, The George Washington University. 

    Machine Learning with Simulation Models and Remote Sensing Data to Constrain the 
    Terrestrial Carbon Cycle, Donovan Murphy, B.S. candidate, University of Maryland. 