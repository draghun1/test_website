---
title: <Name>
subtitle: <Title>
---

### Education: <Highest degree, school>

![image alt text](/img/<personal image>.jpeg)

**Email**: <email>

**Projects (Role)**: 	
-   Role 1 (Team + Description)
-   Role 2 (Team +Description)

**Expertise**:  
-   Expertise 1 
-   Expertise 2 

**Bio**: <Description of past experiences, interests and current hobbies>
