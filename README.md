## Download Hugo

To work locally with this project, you'll have to follow the steps below:

1. Clone or download this project
2. [Install](https://gohugo.io/getting-started/installing/) Hugo
3. change directory to the root of this project
4. Preview your project: `hugo server`
5. your site can be accessed under `localhost:1313/hugo/`.

Read more at Hugo's [documentation][].

## Modify website content 

**To add menu item on the main page: modify config.toml**
1.  In order to add an item to the menu bar, add this:
    ```
    [[menu.main]]
    identifier = "<menu item>"
    url = "<markdown folder>/<markdown file>"
    name = "<Menu Item>"
    weight = <item number on the menu bar>
    ```
2.  In order to add a project under 'work' menu, add this:
    ```
    [[menu.main]]
    parent = "work"
    name = "<project name for display>"
    url = "work/<project markdown file>"
    weight = <item number in drop down menu>
    ```
3.  In order to add project under 'courses' menu, add this:
    ```
    [[menu.main]]
    parent = "courses"
    name = "<course name for display>"
    url = "courses/<course markdown file>"
    weight = <item number in drop down menu>
    ```

**To change high-level introduction of teammembers:**
1.  Open about.md under content/about/ 
2.  Add person's name under members title, (```e.g. ## <name>```)
3.  Add a brief description relating person's role in the team, and their contributions. 


**To change in-depth introductions of teammembers:**
1.  Create a file, or edit a file corresponding to the member's name under content/about/. (```e.g. deepthi.md ```)
2.  To add images corresponding to person under ```themes/beautifulhugo/static/img```
3.  Add content corresponding to the template provided "about_template.md" to the markdown file


**To change introductions of work:**
1.  Create a file, or edit a file corresponding to the work (```e.g. snowmelt.md```)
2.  To add images corresponding to work under ```themes/beautifulhugo/static/img```
3.  Add content corresponding to the template provided "work_template.md" to the markdown file


**To modify theme (html and css) look under themes/beatifulhugo**
